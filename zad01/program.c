#include <stdio.h>
#include <stdlib.h>
#define BUFFOR_SIZE 80

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h> /*deklaracje standardowych funkcji uniksowych (fork(), write() itd.)*/

double sum(double *vector, int n)
{
    int i;
    double sum = 0.0f;
    for (i = 0; i < n; i++)
    {
        sum += vector[i];
    }
    return sum;
}

void on_usr1(int signal)
{
    printf("Otrzymałem USR1\n");

}

int main(int argc, char **argv)
{
    FILE *f = fopen("vector.dat", "r");
    char buffor[BUFFOR_SIZE + 1];
    double *vector;
    int n;
    int i;
    fgets(buffor, BUFFOR_SIZE, f);
    n = atoi(buffor);
    vector = malloc(sizeof(double) * n);
    printf("Vector has %d elements\n", n);
    for (i = 0; i < n; i++)
    {
        fgets(buffor, BUFFOR_SIZE, f);
        vector[i] = atof(buffor);
    }
    fclose(f);
    printf("v = [ ");
    for (i = 0; i < n; i++)
    {
        printf("%f ", vector[i]);
    }
    printf("]\n");

    printf("Suma elementów w wektorze = %f\n", sum(vector, n));

    key_t key; /* key to be passed to shmget() */
    key_t key_range; /* key to be passed to shmget() */
    key_t key_result; /* key to be passed to shmget() */

    key = 1000;
    key_range = 1001;
    key_result = 1002;




    int ile_dzieci = 5;
    if (argc == 2)
    {
        ile_dzieci = atoi(argv[1]);
        printf("Creating %d child processes!\n", ile_dzieci);
    }

    pid_t pids[ile_dzieci];

    int id = -1;

    pid_t pid;
    printf("Moj PID = %d\n", getpid());
    for (i = 0; i < ile_dzieci; i++)
    {
        switch (pid = fork())
        {
        case -1:
            fprintf(stderr, "Blad w fork\n");
            return EXIT_FAILURE;
        case 0: /*proces potomny*/
            id = i;
            printf("Jestem procesem potomnym. PID = %d\n \
                Wartosc przekazana przez fork() = %d\n \
                Moje id = %d \n",
                   getpid(), pid, id);
            sigset_t mask; /* Maska sygnałów */

            /* Konfiguracja obsługi sygnału USR1 */
            struct sigaction usr1;
            sigemptyset(&mask); /* Wyczyść maskę */
            usr1.sa_handler = (&on_usr1);
            usr1.sa_mask = mask;
            usr1.sa_flags = SA_SIGINFO;
            sigaction(SIGUSR1, &usr1, NULL);
            pause();
            return 0;
            break;
        default:
            pids[i] = pid;
            printf("Jestem procesem macierzystym. PID = %d\n \
                Wartosc przekazana przez fork() = %d\n",
                   getpid(), pid);
        }
    }
    if (id = -1)
    {
        int shmflg; /* shmflg to be passed to shmget() */
        int shmid_data; /* return value from shmget() */
        int shmid_range; /* return value from shmget() */
        int shmid_result; /* return value from shmget() */
        int size; /* size to be passed to shmget() */
        double *shared_vector;
        int * shared_ranges;

        size = sizeof(double) * n;

        shmflg = IPC_CREAT | 0666;

        if ((shmid_data = shmget (key, size, shmflg)) == -1)
        {
            perror("shmget: shmget failed");
            exit(1);
        }
        else
        {
            (void) fprintf(stderr, "shmget: shmget returned %d\n", shmid_data);
        }
        if ((shared_vector = shmat(shmid_data, NULL, 0)) == (double *) -1)
        {
            perror("shmat");
            exit(1);
        }

        for (i = 0; i <n ; i++)
        {
            shared_vector[i] = vector[i];
            printf("Added to shared_vector: %lf \n", shared_vector[i]);
        }
        if ((shmid_result = shmget (key_result, sizeof(double) * ile_dzieci, shmflg)) == -1)
        {
            perror("shmget: shmget failed");
            exit(1);
        }

        if ((shmid_range = shmget (key_range, sizeof(int) * ile_dzieci, shmflg)) == -1)
        {
            perror("shmget: shmget range failed");
            exit(1);
        }

        if ((shared_ranges = shmat(shmid_range, NULL, 0)) == (int *) -1)
        {
            perror("shmat");
            exit(1);
        }
        int scope = n/ile_dzieci;

        shared_ranges[0] = 0;
        for (int i= 1; i <ile_dzieci ; i++)
        {
            shared_ranges[i] = shared_ranges[i-1] + scope;
            if (i == ile_dzieci -1)
                shared_ranges[i] = n-1;
            printf("Range %d: <%d - %d>  \n", i, shared_ranges[i-1], shared_ranges[i]);
        }



        sleep(1);
        for (int i = 0; i< ile_dzieci; i++)
        {
            kill(pids[i], SIGUSR1);
        }


    }
    for (i = 0; i < ile_dzieci; i++)
    {
        wait(0);
    }


    return 0;
}
