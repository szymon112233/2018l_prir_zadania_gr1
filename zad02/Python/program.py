#!/usr/bin/python

import threading
import time
import numpy
import sys
import math
import itertools

class myThread (threading.Thread):
	def __init__(self, id, coords):
		threading.Thread.__init__(self)
		self.id = id
		self.coords = coords
	def run(self):
		global frobenius_norm
		print("Starting Thread-" + str(self.id) + '[ ' + str(self.coords) + ' ]')
		sum = 0.
		for x in range(len(self.coords)):
			res = multiply(self.coords[x][0], self.coords[x][1])
			sum += res * res
		# Get lock to synchronize threads
		threadLock.acquire()
		frobenius_norm += sum
		# Free lock to release next thread
		threadLock.release()
		print("Exiting Thread-" + str(self.id) + '[ ' + str(self.coords) + ' ]')

def multiply(i, j):
	sum = 0.
	for x in range(len(b)):
		sum += a[i][x] * b[x][j]
	result[i][j] = sum
	return sum

def is_multiplying_allowed(a, b):
	if len(a[0]) != len(b):
		return False
	return True

threadLock = threading.Lock()
threadsNumber = int(sys.argv[1])
a = numpy.loadtxt("A.txt", dtype='float')
b = numpy.loadtxt("B.txt", dtype='float')
if not is_multiplying_allowed(a, b):
	print("Matrices are incorrect.")
	exit(1)

result = numpy.zeros((len(a), len(b[0])), dtype='float')

threads = []

cells = len(a) * len(b[0])
if threadsNumber > cells:
	print("Zbyt dużo wątków")
	exit(1)
toCalculate = int(cells / threadsNumber)
rest = cells - (cells % threadsNumber)
counter = 0
frobenius_norm = 0.
id = 1
# Create threads and append them to list
cell_list = list(itertools.product(range(len(a)), range(len(b[0]))))
cells_to_calculate = []
for i in range(threadsNumber):
	cells_to_calculate.append([])

for i in range(toCalculate * threadsNumber):
	cells_to_calculate[int(i / toCalculate)].append(cell_list[i])

cells_to_calculate[threadsNumber - 1] += cell_list[rest:]

for i in range(threadsNumber):
	thread = myThread(i + 1, cells_to_calculate[i])
	threads.append(thread)

# Start all threads
for t in threads:
	t.start()

# Wait for all threads to complete
for t in threads:
	t.join()

print("Result matrix:")
print(result)

print(math.sqrt(frobenius_norm))