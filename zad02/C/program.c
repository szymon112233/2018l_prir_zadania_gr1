#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

double **A;
double **B;
double **C;
int *indexes;
int maxIndex;
int maxObliczen;
int rozmiar_Ca;
int rozmiar_Cb;
int rozmiar_Ab;

pthread_mutex_t mutex_c = PTHREAD_MUTEX_INITIALIZER;

void *thread_function(int myIndex)
{
    int start_index = indexes[myIndex];
    int stop_index = 0;
    if (myIndex != maxIndex)
        stop_index = (indexes[myIndex +1] -1);
    else
        stop_index = maxObliczen;
    printf("myIndex = %d, licze od %d do %d\n", myIndex, start_index, stop_index);
    fflush(stdout);

    int i;
    int a,b;
    int k;
    double s;
    for (i = start_index; i<= stop_index; i++)
    {
        a = i/rozmiar_Ca;
        b = i%rozmiar_Ca;
        printf("myIndex = %d, licze element: %d,%d\n", myIndex, a,b);
        fflush(stdout);

        s = 0;
        printf("myIndex = %d, rozmiar_Ab= %d\n", myIndex, rozmiar_Ab);
        fflush(stdout);
        for(k=0; k<rozmiar_Ab-1; k++)
        {
            printf("myIndex = %d, Element A: %d,%d = %lf\n", myIndex, a,k, A[a][k]);
            fflush(stdout);
            printf("myIndex = %d, Element B: %d,%d = %lf\n", myIndex, k,b, B[k][b]);
            fflush(stdout);
            s+=A[a][k]*B[k][b];
            printf("myIndex = %d, sum = %lf\n", myIndex, s);
            fflush(stdout);
        }
        printf("myIndex = %d, Lockuje mutex!\n", myIndex);
        fflush(stdout);
        pthread_mutex_lock(&mutex_c);
        C[a][b] = s;
        printf("myIndex = %d, Element: %d,%d = %lf\n", myIndex, a, b, s);
        fflush(stdout);
        pthread_mutex_unlock(&mutex_c);



    }

    return NULL;
}

void mnoz(double**A, int a, int b, double**B, int c, int d, double**C)
{
    int i, j, k;
    double s;
    for(i=0; i<a; i++)
    {
        for(j=0; j<d; j++)
        {
            s = 0;
            for(k=0; k<b; k++)
            {
                s+=A[i][k]*B[k][j];
            }
            C[i][j] = s;
        }

    }
}

print_matrix(double**A, int m, int n)
{
    int i, j;
    printf("[");
    for(i =0; i< m; i++)
    {
        for(j=0; j<n; j++)
        {
            printf("%f ", A[i][j]);
        }
        printf("\n");
    }
    printf("]\n");
}
int main(int argc, char **argv)
{
    FILE *fpa;
    FILE *fpb;

    int ma, mb, na, nb;
    int i, j;
    double x;

    fpa = fopen("A.txt", "r");
    fpb = fopen("B.txt", "r");
    if( fpa == NULL || fpb == NULL )
    {
        perror("błąd otwarcia pliku");
        exit(-10);
    }

    fscanf (fpa, "%d", &ma);
    fscanf (fpa, "%d", &na);


    fscanf (fpb, "%d", &mb);
    fscanf (fpb, "%d", &nb);

    printf("pierwsza macierz ma wymiar %d x %d, a druga %d x %d\n", ma, na, mb, nb);

    rozmiar_Ab = na;

    if(na != mb)
    {
        printf("Złe wymiary macierzy!\n");
        return EXIT_FAILURE;
    }

    /*Alokacja pamięci*/
    A = malloc(ma*sizeof(double));
    for(i=0; i< ma; i++)
    {
        A[i] = malloc(na*sizeof(double));
    }

    B = malloc(mb*sizeof(double));
    for(i=0; i< mb; i++)
    {
        B[i] = malloc(nb*sizeof(double));
    }

    /*Macierz na wynik*/
    C = malloc(ma*sizeof(double));
    for(i=0; i< ma; i++)
    {
        C[i] = malloc(nb*sizeof(double));
    }

    printf("Rozmiar C: %dx%d\n", ma, nb);
    rozmiar_Ca = ma;
    rozmiar_Cb = nb;
    for(i =0; i< ma; i++)
    {
        for(j = 0; j<na; j++)
        {
            fscanf( fpa, "%lf", &x );
            A[i][j] = x;
        }
    }

    printf("A:\n");
    print_matrix(A, ma, mb);

    for(i =0; i< mb; i++)
    {
        for(j = 0; j<nb; j++)
        {
            fscanf( fpb, "%lf", &x );
            B[i][j] = x;
        }
    }

    printf("B:\n");
    print_matrix(B, mb, nb);

    int ile_obliczen = ma * nb;

    maxObliczen = ile_obliczen -1;

    printf("To bedzie %d elementow do obliczenia!\n", ile_obliczen);

    int ile_watkow = ma * nb;

    if (argc == 2)
    {
        ile_watkow = atoi(argv[1]);
    }
    maxIndex = ile_watkow -1;

    int obliczenPerWatek = ile_obliczen/ile_watkow;
    printf("To bedzie %d elementow na watek!\n", obliczenPerWatek);

    indexes = malloc (ile_watkow * sizeof(int));

    indexes[0] = 0;

    for (i = 1; i<ile_watkow; i++)
    {
        indexes[i] = i * obliczenPerWatek;
    }

    printf("Tworze %d watkow!\n", ile_watkow);

    pthread_t *threads = malloc(ile_watkow * sizeof(pthread_t));


    for (i = 0; i < ile_watkow ; i++)
    {
        printf("Tworze %d watek!\n", i);
        if ( pthread_create( &threads[i], NULL, thread_function, i) )
        {
            printf("error creating thread.");
            return 0;
        }
    }

    //mnoz(A, ma, na, B, mb, nb, C);

    for (i = 0; i < ile_watkow ; i++)
    {
        if ( pthread_join( threads[i], NULL) !=0 )
        {
            printf("error joining thread.");
            return 0;
        }
    }

    printf("C:\n");
    print_matrix(C, ma, nb);


    for(i=0; i<na; i++)
    {
        free(A[i]);
    }
    free(A);

    for(i=0; i<nb; i++)
    {
        free(B[i]);
    }
    free(B);

    for(i=0; i<nb; i++)
    {
        free(C[i]);
    }
    free(C);


    fclose(fpa);
    fclose(fpb);


    return 0;
}
